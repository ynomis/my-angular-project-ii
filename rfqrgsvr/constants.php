<?php

$LOC_REQ_FIELDS = array(
        'location_name' => null,
        'npanxx' => null,
        'address' => null,
        'building' => null,
        'suite' => null,
        'city' => null,
        'state' => null,
        'zip' => null,
);

$QUOTE_SQ_FIELDS = array(
        'id' => null,
        'term' => null,
//         'api_quote_id' => null,
//         'stated_quote_date' => null,
//         'stated_expire_date' => null,
        'promotion' => null,
        'quote_complete' => null,
        'provider_id' => null,
//         'info_requested' => null,
//         'mixed_providers' => null,
        'vendor_name' => null,
        'hub_loc_num' => null,
        'hub_node_type' => null,
);

$LOC_QUOTE_FIELDS = array(
        'id' => null,
//         'api_quote_id' => null,
        'comments' => null,
);