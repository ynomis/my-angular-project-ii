<?php

defined('DOC_ROOT') or define('DOC_ROOT', realpath(__DIR__ . "/../../"));

// ini_set('display_errors', 1);
// error_reporting(E_ALL);

require DOC_ROOT . '/inc/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// Autoload model classes in Mstr namespace
require_once(DOC_ROOT . '/models/autoload.php');

//With default settings
$app = new \Slim\Slim(array(
	'debug' => true,
));
//With custom settings
/*
$app = new Slim(array(
    'log.enable' => true,
    'log.path' => './logs',
    'log.level' => 4
    //'view' => 'MyCustomViewClassName'
));
*/

// $app->add(new \Slim\Middleware\ContentTypes());

$app->get('/macomps', function() use ($app) {

	try {
		$macomps = \Mstr\MACompany::all(array(
		    'select' => 'id, company_name',
			'conditions' => array("inactive = ?", 'f'),
		    'order' => 'company_name',
		));
	} catch (\Exception $e) {
		$app->halt(501, "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$out = array();
	foreach ($macomps as $_com) {
		$out [] = $_com->to_array();
	}
	$response->body(json_encode($out));

});

$app->get('/rfqbases/:macompid', function($macompid) use ($app) {
	$bases = \Mstr\RFQTest\Base::all(array(
		'conditions' => array('ma_company_id = ? AND active = ?', $macompid, 't'),
	));
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$out = array_map(function($item) { return $item->to_array(); }, $bases);

	$response->body(json_encode($out));

});

$app->get('/rfqrecent/:macompid/:baserfqnumber', function($macompid, $baserfqnumber) use ($app) {
	$recent = \Mstr\RFQTest\TestInstance::first(array(
	    'joins' => "INNER JOIN (SELECT ma_company_id, base, MAX(created) latest FROM rfqtest_history GROUP BY ma_company_id, base) s
	                   ON rfqtest_history.ma_company_id = s.ma_company_id AND rfqtest_history.base = s.base AND rfqtest_history.created = s.latest
	                INNER JOIN rfqtest_base ON rfqtest_base.rfqnumber = rfqtest_history.base AND rfqtest_base.ma_company_id = rfqtest_history.ma_company_id",
		'conditions' => array('rfqtest_history.ma_company_id = ? AND rfqtest_history.base = ?', $macompid, $baserfqnumber),
	));
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$out = ($recent instanceof \Mstr\RFQTest\TestInstance) ? $recent->to_array() : null;

	$response->body(json_encode($out));

});

$app->get('/quotedetail/:rfqnumber', function($rfqnumber) use ($app) {
	$locqs = \Mstr\Quote\LocationQuote::all(array(
	    'joins' => "INNER JOIN quote_sq qs ON quote_sq_per_loc.quote_sq_id = qs.id
	                INNER JOIN rfq_idx ri ON qs.rfq_idx_id = ri.id
	                INNER JOIN rfq ON ri.rfq_id = rfq.id",
        'conditions' => array('rfq.tracking_number = ?', $rfqnumber),
	));

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

    require_once 'constants.php';

	$out = array();
	foreach ($locqs as $_lq) {
		$_temp = array();
		$_temp['location_requirement'] = array_intersect_key($_lq->loc_requirement->to_array(), $LOC_REQ_FIELDS);
		$_temp['parent_quote'] = array_intersect_key($_lq->quote->to_array(), $QUOTE_SQ_FIELDS);
		$_temp['location_quote'] = array_intersect_key($_lq->to_array(), $LOC_QUOTE_FIELDS);
		$_temp['skus'] = array();
		foreach ($_lq->skus as $sku) {
		    $key = $sku->vendor_id . '|' . $sku->term . '|' . $sku->sku . '|' . $sku->market . '|' . $sku->amount_type;
		    $_temp['skus'][$key] = $sku->to_array();
		}

		$out[] = $_temp;
	}

	$response->body(json_encode($out));

});

$app->post('/createrunner/:baserfqnumber', function($baserfqnumber) use ($app) {

    $ma_company_id = $app->request()->params('macompid');
	$newrunner     = $app->request()->params('runner');

	$newItem = \Mstr\RFQTest\TestInstance::create(array(
		'ma_company_id'   => $ma_company_id,
	    'base'            => $baserfqnumber,
        'runner'          => $newrunner,
	    'created'         => new \ActiveRecord\DateTime(),
	));

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	$response->body(json_encode($newItem->to_array()));

});

$app->post('/createbase/:macompid', function($macompid) use ($app) {
	$newbase = array(
	    'ma_company_id' => $macompid,
		'rfqnumber' => $app->request()->params('rfqnumber'),
		'description' => $app->request()->params('description'),
	    'active' => true,
	);

	$response = $app->response();
	$response['Content-Type'] = 'application/json';

	try {
    	$newBaseObj = \Mstr\RFQTest\Base::create($newbase);

    	$response->status(200);
		$response->body(json_encode($newBaseObj->to_array()));
	} catch(\Exception $e) {
	    $response->status(400);
	    $response->body(json_encode(array('message' => $e->getMessage())));
	}

});

$app->delete('/deletebase/:rfqid', function($rfqid) use ($app) {
	$response = $app->response();
	$response['Content-Type'] = 'application/json';

    try {
        $rfqBase = \Mstr\RFQTest\Base::find($rfqid);
        $rfqnum = $rfqBase->rfqnumber;
        $rfqBase->delete();

        \Mstr\RFQTest\TestInstance::delete_all(
            array('conditions' => array('base = ?', $rfqnum))
        );

        $response->status(200);
    } catch(\Exception $e) {
        $response->status(400);
        $response->body(json_encode(array('message' => $e->getMessage())));
    }
    
});


$app->run();

