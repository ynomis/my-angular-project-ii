'use strict';

var serviceUrl = 'http://devvm.democompany.com'; // dummy

window.rfqTest = angular.module('rfqtesterApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.select2',
  'ui.bootstrap'
], function($httpProvider) {
// Use x-www-form-urlencoded Content-Type
$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

// Override $http service's default transformRequest
$httpProvider.defaults.transformRequest = [function(data)
{
  /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */
  var param = function(obj)
  {
    var query = '';
    var name, value, fullSubName, subName, subValue, innerObj, i;

    for(name in obj)
    {
      value = obj[name];

      if(value instanceof Array)
      {
        for(i=0; i<value.length; ++i)
        {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object)
      {
        for(subName in value)
        {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
      {
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
      }
    }

    return query.length ? query.substr(0, query.length - 1) : query;
  };

  return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
  });

rfqTest.value('localStorage', window.localStorage);

rfqTest.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })
    .when('/compare', {
      templateUrl: 'views/compare.html',
      controller: 'CompareCtrl'
    })
    .when('/diffview/:recid', {
      templateUrl: 'views/diff.html',
      controller: 'DiffviewCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
});

rfqTest.factory('RfqTestService', function($resource) {
    return $resource('/test/rfqrgsvr/:action/:values/:param', {values: '@values'}, {
      getMAs:         { method: 'GET',    params: {action: 'macomps'},     isArray: true },
      getBases:       { method: 'GET',    params: {action: 'rfqbases'},    isArray: true },
      getRunners:     { method: 'GET',    params: {action: 'rfqrecent', param: '@param'} },
      getQuoteDetail: { method: 'GET',    params: {action: 'quotedetail'}, isArray: true },
      createRunner:   { method: 'POST',   params: {action: 'createrunner'} },
      createBase:     { method: 'POST',   params: {action: 'createbase'} },
      deleteBase:     { method: 'DELETE', params: {action: 'deletebase'} }
    });
});

rfqTest.directive('diffPane', function(RfqTestService, $rootScope) {
    return {
		restrict: 'E',
		scope: {
		    rec: '=testrec',
		    macompid: '@'
		},
		templateUrl: 'views/diffpane.html',
		link: function(scope, elem, attrs) {
		    scope.message = "Loading data...";
		    scope.skuCollapse = true;
	
		    scope.rec.$promise.then(function(result) {
		    	scope.left = RfqTestService.getQuoteDetail({values: result.base});
		    	scope.right = RfqTestService.getQuoteDetail({values: result.runner});
		    	
		    	scope.right.$promise.then(function(result) {
		    		if (!result.length) {
		    			scope.message = 'Quotes are not avaiable at the moment, try again later.';
		    		};
		    	});
		    	
		    });
	
		    scope.mergeKeys = function(arrLeft, arrRight) {
				arrLeft = arrLeft || [];
				arrRight = arrRight || [];
				return _.without(_.union(Object.keys(arrLeft), Object.keys(arrRight)), '$promise', '$resolved');
		    }
	
		    scope.newTest = function(baserfq) {
		    	$rootScope.$emit('KICK_OFF_NEW_RFQ_TEST', baserfq);
		    };
	
		    scope.swapBase = function(oldbase, newbase) {
		    	$rootScope.$emit('KICK_OFF_SWAP_RFQ_BASE', oldbase, newbase);
		    };
		}
    };
});

/*
 * Angular ng-repeat doesn't honor object key order in object.
 * Instead it re-order all keys in probably alphabetic order.
 * following is the way to enforce it.
 */
rfqTest.filter('natualOrder', function() {
    return function(input) {
    	if (!input) return [];
    	return Object.keys(input);
    };
});

