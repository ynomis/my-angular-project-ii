'use strict';

rfqTest.controller('CompareCtrl', function ($scope, $location, RfqTestService, $rootScope, $http) {
    $scope.isCollapsed = false;
    $scope.btnBaseCollapse = 1;

	if (!localStorage['macomp']) {
		$location.path('/');
		return false;
	}

    var macomp = JSON.parse(localStorage['macomp']);
    $scope.macompid		= macomp.id;
    $scope.companyname	= macomp.company_name;

    $scope.bases = RfqTestService.getBases({values: $scope.macompid});

    $scope.newbase = {};
    $scope.addNewRfqBase = function(newbase) {
	var newrfqnum = $.trim(newbase.rfqnumber);

	if (!newrfqnum) {
	    alert('You new RFQ number is empty?');
	}else if (_.contains(_.pluck($scope.bases, 'rfqnumber'), newrfqnum)) {
	    alert("The RFQ# " + newrfqnum + " already exists.");
	} else {
	    RfqTestService.createBase($.extend({values: $scope.macompid}, newbase), 
		function(success) {
		    $scope.bases.push(success);
		    $scope.newbase = {};
		},
		function(failed) {
		    console.log(failed.data.message);
		}
	    );
	}
    }

    $scope.deleteRfqBase = function(deleteRfqIndex) {
		if (confirm('All recent tests will be removed, Are you sure?')) {
		    RfqTestService.deleteBase({ values: $scope.bases[deleteRfqIndex].id },
		    	function(success) {
			    	var _rfqnum = $scope.bases[deleteRfqIndex]['rfqnumber'];
			    	$scope.bases.splice(deleteRfqIndex, 1);
		        },
		        function(failed) {
		    	    console.log(failed.data.message);
		        }
		    );
		} else {
		    return false;
		}
    }

    var findBaseIndex = function(rfqnumber) {
		for (var i = 0; i < $scope.bases.length; i++) {
		    if (rfqnumber == $scope.bases[i]['rfqnumber'])
			return i;
		}
		return -1;
    }

    $scope.cloneRfq = function(rfqbase) {
    	if (confirm('This action will clone current base RFQ, existing test result will be replaced, yes?' + 
	    	'\nYou may need to login to MA system to complete this action.')) {
    		$http.post('/rfq_manager/ajax.php', {ajaxFunction: 'importRFQ', rfq_number: rfqbase})
    		.then(function(result) {
		      if (!result.data.success) {
		    	  alert('Something is wrong...');
		      } else {
				  RfqTestService.createRunner({ values: rfqbase, runner: result.data.newRfqNum, macompid: $scope.macompid },
				  function(success) {
					  // don't have to do anything
				  },
				  function(failed) {
				      alert(failed);
				  });
		      }
		});
    };};

    $rootScope.$on('KICK_OFF_NEW_RFQ_TEST', function(event, baserfqnum) {
	$scope.cloneRfq(baserfqnum);
    });

    $rootScope.$on('KICK_OFF_SWAP_RFQ_BASE', function(event, oldbase, newbase) {
	var deleteBaseIndex = findBaseIndex(oldbase);
	if (deleteBaseIndex > -1) {
	    if (false === $scope.deleteRfqBase(deleteBaseIndex)) {
		return false;
	    } else {
		// add new base and new test
		$scope.addNewRfqBase({rfqnumber: newbase, description: 'Replaced RFQ Test Base#:' + oldbase});
		$scope.cloneRfq(newbase);
	    }
	}


    });

});
