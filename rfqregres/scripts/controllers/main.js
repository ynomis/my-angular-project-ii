'use strict';

rfqTest.controller('MainCtrl', function ($scope, RfqTestService, localStorage) {
  	$scope.selectOption = {
  		allowClear: true,
  		placeholder: 'Pick a Company'
  	};

  	$scope.macomps = RfqTestService.getMAs({}, function() {
        $scope.macomp = localStorage['macomp'];
  	});

  	$scope.startTest = function() {
        localStorage['macomp'] = $scope.macomp;
  	};

});
