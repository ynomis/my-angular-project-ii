'use strict';

rfqTest.controller('DiffviewCtrl', function ($scope, $location, $routeParams, RfqTestService) {
	if (!localStorage['macomp']) {
		$location.path('/');
		return false;
	}

	var recentId = $routeParams.recid;

	var macomp = JSON.parse(localStorage['macomp']);
    $scope.macompid		= macomp.id;
    $scope.companyname	= macomp.company_name;

    $scope.recent = RfqTestService.getRunners({values: $scope.macompid, param: recentId});

});
